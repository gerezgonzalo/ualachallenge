package gonzalo.dev.ualachallenge.common.list

import android.view.View

interface ICardClick<Any> {

    fun onItemClicked(item: Any)

    @JvmDefault
    fun onItemAction(item: Any, view: View) {
    }
}

package gonzalo.dev.ualachallenge.common

import androidx.annotation.Keep
import java.util.concurrent.TimeUnit

@Keep
/**
 * specifies the possible kinds of elements which can be annotated with the annotation
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
/**
 * specifies whether the annotation is stored in the compiled class files and whether it's visible
 * through reflection at runtime
 */
@Retention(AnnotationRetention.RUNTIME)

annotation class Timeout(val timeout: Long, val timeUnit: TimeUnit)
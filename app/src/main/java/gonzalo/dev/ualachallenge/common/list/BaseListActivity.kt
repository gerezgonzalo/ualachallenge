package gonzalo.dev.ualachallenge.common.list

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import gonzalo.dev.ualachallenge.R
import gonzalo.dev.ualachallenge.common.mvvm.BaseActivity
import gonzalo.dev.ualachallenge.common.mvvm.BaseViewModel
import gonzalo.dev.ualachallenge.databinding.ActivityBaseListBinding

/**
 * @param DS The data set type.
 * @param VM The view model type.
 */
abstract class BaseListActivity<DS, VM : BaseViewModel> : BaseActivity<VM>(), ICardClick<DS> {

    private val binding by lazy { ActivityBaseListBinding.inflate(layoutInflater) }

    /**
     * Boolean used to decide if we should open the disclaimer screen in order to show to the user
     * how does the filter work.
     */
    private var disclaimerShowed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.baseList.layoutManager = mLayoutManager
        binding.baseList.setHasFixedSize(true)

        binding.baseRefresh.setOnRefreshListener {
            setRefreshing(true)
            onSwipe()
        }

    }

    protected fun canRefresh(value: Boolean) {
        binding.baseRefresh.isEnabled = value
    }

    protected fun getList() = binding.baseList


    /**
     * Show the swipe progress animation depending on the boolean value received.
     */
    protected fun setRefreshing(value: Boolean) {
        binding.baseRefresh.isRefreshing = value
    }

    override fun getRootView(): View {
        return binding.root
    }

    abstract fun onSwipe()

    abstract fun onQueryTextChanged(approximation: String)

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.base_list_menu, menu)
        menu?.findItem(R.id.search)?.expandActionView()
        val searchView = menu?.findItem(R.id.search)?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                println("fata Cambiando    $newText")
                newText?.takeIf { it.length > 2 }?.also {
                    onQueryTextChanged(it)
                }
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                //Do nothing.
                return false
            }
        })
        return true
    }
}
package gonzalo.dev.ualachallenge.common.util

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    const val TAG = "fataUtils"

    /**
     * Return the system date in the dd-MM-yyyy HH:mm format.
     */
    fun getSystemDate(): String? {
        Log.i(TAG, "getSystemDate: ")
        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        return sdf.format(GregorianCalendar.getInstance().time)
    }
}
package gonzalo.dev.ualachallenge.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import gonzalo.dev.core.data.repository.HomeRepository
import gonzalo.dev.core.usecase.GetMeals
import gonzalo.dev.ualachallenge.BuildConfig
import gonzalo.dev.ualachallenge.common.Timeout
import gonzalo.dev.ualachallenge.home.HomeService
import gonzalo.dev.ualachallenge.home.datasource.DataSourceImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
@InstallIn(ActivityComponent::class)
class AppModule {

    @Provides
    fun provideHttpInterceptor() = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    fun provideOkHttpInstance(interceptor: HttpLoggingInterceptor, timeout: Timeout) =
        OkHttpClient.Builder()
            .connectTimeout(timeout.timeout, TimeUnit.SECONDS)
            .readTimeout(timeout.timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout.timeout, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .build()

    @Provides
    fun provideGsonInstance() = GsonBuilder()
        .enableComplexMapKeySerialization()
        .serializeNulls()
        .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        .setPrettyPrinting()
        .setVersion(1.0)
        .create()

    @Provides
    fun provideRetrofitInstance(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(provideGsonInstance()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    @Provides
    fun provideHomeService(): HomeService {
        val timeOut = HomeService::class.java.getAnnotation(Timeout::class.java)
        return provideRetrofitInstance(provideOkHttpInstance(provideHttpInterceptor(), timeOut!!))
            .create(HomeService::class.java)
    }

    @Provides
    fun provideHomeRepository() = HomeRepository(DataSourceImpl(provideHomeService()))

    @Provides
    fun provideGetMealsUseCase() = GetMeals(provideHomeRepository())

    /* @Provides
     fun providePostService(): PostsService {
         val timeOut = PostsService::class.java.getAnnotation(Timeout::class.java)
         return provideRetrofitInstance(provideOkHttpInstance(provideHttpInterceptor(), timeOut!!))
             .create(PostsService::class.java)
     }

     @Provides
     fun provideRepository() = PostRepository(PostDataSourceImpl(providePostService()))

     @Provides
     fun provideGetPostUseCase(): GetTopPosts = GetTopPosts(provideRepository())*/
}
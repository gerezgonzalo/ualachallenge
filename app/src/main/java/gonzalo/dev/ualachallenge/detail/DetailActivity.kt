package gonzalo.dev.ualachallenge.detail

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import gonzalo.dev.core.domain.model.Meal
import gonzalo.dev.ualachallenge.common.mvvm.BaseActivity
import gonzalo.dev.ualachallenge.common.mvvm.BaseViewModel
import gonzalo.dev.ualachallenge.databinding.ActivityDetailBinding

class DetailActivity : BaseActivity<BaseViewModel>() {
    companion object {
        const val EXTRA = "da_extra"
    }

    private val binding by lazy { ActivityDetailBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val selectedMeal = intent.getSerializableExtra(EXTRA) as Meal

        binding.detailPrimaryText.text = selectedMeal.strMeal
        binding.detailBody.text = selectedMeal.strInstructions
    }

    override fun createViewModelFactory(): BaseViewModel {
        val vm: BaseViewModel by viewModels()
        return vm
    }

    override fun getRootView(): View {
        return binding.root
    }
}
package gonzalo.dev.ualachallenge.home

import gonzalo.dev.core.data.dto.MealResponse
import gonzalo.dev.ualachallenge.common.Timeout
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

@Timeout(timeout = 15, timeUnit = TimeUnit.SECONDS)
interface HomeService {

    @GET("search.php")
    suspend fun fetchPosts(@Query("s") mealName: String): MealResponse?
}
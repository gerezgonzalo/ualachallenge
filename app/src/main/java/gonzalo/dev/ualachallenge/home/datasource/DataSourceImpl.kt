package gonzalo.dev.ualachallenge.home.datasource

import androidx.annotation.CheckResult
import gonzalo.dev.core.data.dto.MealResponse
import gonzalo.dev.core.data.repository.HomeDataSource
import gonzalo.dev.core.domain.model.Meal
import gonzalo.dev.ualachallenge.home.HomeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class DataSourceImpl(private val homeService: HomeService) : HomeDataSource {

    /**
     * Fetch the data from the network source.
     *
     * @param mealName The meal name. Is not necessary the complete name, this could be an
     * approximation.
     */
    @CheckResult
    @ExperimentalCoroutinesApi
    override suspend fun fetchMeals(mealName: String): Flow<MealResponse?> {
        return flow {
            val response = homeService.fetchPosts(mealName)
            emit(response)
        }.flowOn(Dispatchers.IO)
    }

    /**
     * Wont implement this method. This is only to be aware about where we should decide about the
     * data source used.
     */
    override suspend fun readMeals(): List<Meal> {
        throw IllegalStateException("This method won't be implemented.")
    }
}
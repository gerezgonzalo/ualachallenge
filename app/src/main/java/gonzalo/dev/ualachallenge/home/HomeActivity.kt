package gonzalo.dev.ualachallenge.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import gonzalo.dev.core.domain.model.Meal
import gonzalo.dev.ualachallenge.common.list.BaseListActivity
import gonzalo.dev.ualachallenge.detail.DetailActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : BaseListActivity<Meal, HomeViewModel>() {
    companion object {
        private const val TAG = "fataHomeActivity"
    }

    private val adapter = HomeAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getList().adapter = adapter

        getViewModel().searchState.observe(this, {
            adapter.setData(ArrayList(it.meals ?: listOf()))
        })
    }

    override fun createViewModelFactory(): HomeViewModel {
        val vm: HomeViewModel by viewModels()
        return vm
    }

    override fun onSwipe() {
        println("fata implement this")
    }

    @ExperimentalCoroutinesApi
    override fun onQueryTextChanged(approximation: String) {
        Log.i(TAG, "onQueryTextChanged: ")
        getViewModel().searchByName(approximation)
    }

    override fun onItemClicked(item: Meal) {
        startActivity(Intent(this, DetailActivity::class.java).apply {
            putExtra(DetailActivity.EXTRA, item)
        })
    }

    override fun onItemAction(item: Meal, view: View) {
        //open image
    }
}
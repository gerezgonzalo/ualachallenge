package gonzalo.dev.ualachallenge.home

import android.app.Application
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import gonzalo.dev.core.data.dto.MealResponse
import gonzalo.dev.core.usecase.GetMeals
import gonzalo.dev.ualachallenge.common.mvvm.BaseViewModel
import gonzalo.dev.ualachallenge.common.util.ErrorUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel @ViewModelInject constructor(
    private val app: Application, private val getMealsUseCase: GetMeals
) : BaseViewModel(app) {
    companion object {
        private const val TAG = "fataHomeViewModel"
    }

    private val _searchState = MutableLiveData<MealResponse>()
    val searchState: LiveData<MealResponse> = _searchState

    /**
     * Search a meal by it's name. The requests to the server will be executed after third inputted
     * word in order avoid the server overload.
     *
     * @param mealName The meal name approximation.
     * @return A meal list matching with the name received.
     */
    @ExperimentalCoroutinesApi
    fun searchByName(mealName: String) {
        setViewStateAsLoading()
        viewModelScope.launch {
            getMealsUseCase.getByName(mealName)
                .catch {
                    Log.i(TAG, "catching error :  ${it.message}")
                    setViewStateAsLayout()
                    errorState.postValue(ErrorUtils.errorMessage(app, it))
                }
                .collect { mealResponse ->
                    mealResponse?.let { _searchState.postValue(it) }
                    setViewStateAsLayout()
                }
        }
    }

}
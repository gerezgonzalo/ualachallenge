package gonzalo.dev.ualachallenge.home

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import gonzalo.dev.core.domain.model.Meal
import gonzalo.dev.ualachallenge.R
import gonzalo.dev.ualachallenge.common.list.BaseAdapter
import gonzalo.dev.ualachallenge.common.list.ICardClick
import gonzalo.dev.ualachallenge.common.util.FrescoUtils

class HomeAdapter(private val callback: ICardClick<Meal>) :
    BaseAdapter<Meal, HomeAdapter.HomeViewHolder>() {

    private var selectedPosition = RecyclerView.NO_POSITION

    override fun setData(dataSet: ArrayList<Meal>) {
        _data = dataSet
        notifyDataSetChanged()
    }

    override fun getItemResource(): Int {
        return R.layout.item_list_post
    }

    override fun getViewHolderInstance(view: View): HomeViewHolder {
        return HomeViewHolder(view)
    }

    override fun onBind(holder: HomeViewHolder, position: Int) {
        holder.item.isSelected = selectedPosition == holder.adapterPosition
        holder.bind(_data[holder.adapterPosition])
    }

    inner class HomeViewHolder(val item: View) : RecyclerView.ViewHolder(item) {

        private val title: TextView by lazy { item.findViewById(R.id.itemTitle) }
        private val body: TextView by lazy { item.findViewById(R.id.itemBodyText) }
        private val thumb: SimpleDraweeView by lazy { item.findViewById(R.id.itemThumb) }

        fun bind(meal: Meal) {
            title.text = meal.strMeal
            body.text = meal.strCategory
            FrescoUtils.setImage(thumb, meal.strMealThumb)

            item.setOnClickListener {
                callback.onItemClicked(meal)
            }

            thumb.setOnClickListener {
                callback.onItemAction(meal, thumb)
            }
        }
    }

}

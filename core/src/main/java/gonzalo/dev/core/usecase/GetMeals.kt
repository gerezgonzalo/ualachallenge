package gonzalo.dev.core.usecase

import gonzalo.dev.core.data.repository.HomeRepository

class GetMeals(private val repository: HomeRepository) {

    suspend fun getByName(mealName: String) = repository.fetchMeals(mealName)
}
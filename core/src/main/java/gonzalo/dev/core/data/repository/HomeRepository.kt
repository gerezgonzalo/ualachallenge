package gonzalo.dev.core.data.repository

class HomeRepository(private val homeDataSource: HomeDataSource) {

    suspend fun fetchMeals(mealName: String) = homeDataSource.fetchMeals(mealName)

    suspend fun readAllMeals() = homeDataSource.readMeals()
}
package gonzalo.dev.core.data.dto

import gonzalo.dev.core.domain.model.Meal

data class MealResponse(val meals: List<Meal>?)

package gonzalo.dev.core.data.repository

import gonzalo.dev.core.domain.model.Meal
import gonzalo.dev.core.data.dto.MealResponse
import kotlinx.coroutines.flow.Flow

interface HomeDataSource {

    suspend fun fetchMeals(mealName: String): Flow<MealResponse?>

    suspend fun readMeals(): List<Meal>
}
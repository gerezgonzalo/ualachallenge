package gonzalo.dev.core.domain.model

import java.io.Serializable

data class Meal(
    val idMeal: Long,
    val strMeal: String,
    val strCategory: String,
    val strMealThumb: String,
    val strInstructions: String
) : Serializable
// FIXME: 04/12/2020 We must use the @Parcelize annotation and implement Parcelable but i'm getting some error.

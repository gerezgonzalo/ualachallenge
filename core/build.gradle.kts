plugins {
    id(Plugins.javaLibrary)
    id(Plugins.kotlin)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation(Dependencies.kotlinStdlib)
    implementation(Dependencies.kotlinxCoroutines)
   // implementation(Dependencies.Google.hilt)
   // implementation(Dependencies.Google.hiltKpt)
}
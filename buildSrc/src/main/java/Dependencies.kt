object Plugins {
    const val kotlin = "kotlin"
    const val javaLibrary = "java-library"
    const val android = "com.android.application"
    const val hilt = "dagger.hilt.android.plugin"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExt = "kotlin-android-extensions"
    const val kotlinKapt = "kotlin-kapt"
}

object AppConfig {
    const val compileSdk = 30
    const val minSdk = 24
    const val targetSdk = 29
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val buildToolsVersion = "30.0.2"
    const val applicatoinId = "gonzalo.dev.ualachallenge"

    const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"

    val compileArgs = listOf("-Xjvm-default=enable")
    val features = setOf("parcelize")
}


object Versions {
    const val appCompat = "1.2.0"
    const val kotlinPlugin = "1.4.10"
    const val coreKtx = "1.3.2"
    const val material = "1.2.1"
    const val constraintLayout = "2.0.4"
    const val legacySupportV4 = "1.0.0"
    const val lifecycleLivedataKtx = "2.2.0"
    const val junit = "4.12"
    const val mockito = "3.3.3"
    const val gradle = "3.5.1"
    const val kotlin = "1.3.60"
    const val retrofit = "2.7.1"
    const val okhttpLoggingInterceptor = "4.5.0"
    const val recyclerView = "1.1.0"
    const val kotlinxCoroutines = "1.3.2"
    const val junitPlatformRunner = "1.0.2"
    const val gson = "2.8.6"
    const val hilt = "2.28-alpha"
    const val hiltArch = "1.0.0-alpha02"
    const val activityKtx = "1.1.0"
    const val fragmentKtx = "1.2.5"
    const val fresco = "2.3.0"
    const val kotlinCoroutines = "1.3.4"
    const val mockitoInline = "2.27.0"
}

object BuildPluginsVersion {
    const val gradlePlugin = "4.1.1"
    const val kotlinPlugin = "1.4.10"
}

object Dependencies {

    const val gradlePlugin = "com.android.tools.build:gradle:${BuildPluginsVersion.gradlePlugin}"
    const val kotlinPlugin =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${BuildPluginsVersion.kotlinPlugin}"
    const val hiltPlugin = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"

    const val kotlinxCoroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinxCoroutines}"
    const val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinPlugin}"

    object Fresco {
        const val fresco = "com.facebook.fresco:fresco:${Versions.fresco}"
    }

    object Google {
        const val material = "com.google.android.material:material:${Versions.material}"
        const val gson = "com.google.code.gson:gson:${Versions.gson}"
        const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
        const val hiltKpt = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
    }

    // When using Kotlin.
    object AndroidX {

        const val hiltLifecycleViewModel =
            "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltArch}"
        const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltArch}"

        const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
        const val androidCompatibility = "androidx.appcompat:appcompat:${Versions.appCompat}"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
        const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"

        const val fragmentExt = "androidx.fragment:fragment-ktx:${Versions.fragmentKtx}"

        const val legacySupport =
            "androidx.legacy:legacy-support-v4:${Versions.legacySupportV4}"
        const val lifecycleLivedataKtx =
            "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycleLivedataKtx}"
        const val lifecycleCompiler =
            "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycleLivedataKtx}"
        const val archViewModel =
            "androidx.lifecycle:lifecycle-viewmodel:${Versions.lifecycleLivedataKtx}"
        const val lifeCicleExt =
            "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleLivedataKtx}"
        const val viewModelExt =
            "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycleLivedataKtx}"

        const val activityExt = "androidx.activity:activity-ktx:${Versions.activityKtx}"
    }

    const val gsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val callAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val logginInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.okhttpLoggingInterceptor}"


    object Test {
        const val androidArch = "android.arch.core:core-testing:2.1.0"
        const val mockitoCore = "org.mockito:mockito-core:${Versions.mockito}"
        const val junit = "junit:junit:${Versions.junit}"
        const val junitPlatformRunner =
            "org.junit.platform:junit-platform-runner:${Versions.junitPlatformRunner}"
        const val kotlinCoroutines =
            "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.kotlinCoroutines}"
        const val mockitoInline = "org.mockito:mockito-inline:${Versions.mockitoInline}"
        const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"
        const val kluent = "org.amshove.kluent:kluent:1.14"

    }
}
